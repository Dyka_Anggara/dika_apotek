<?php

namespace Database\Seeders;

use App\Models\obat;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ObatSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        obat::create([
            'nama_obat' => 'promag',
            'stok' => 99
        ]);
        
        //
    }
}
