<?php

namespace App\Http\Controllers;

use App\Models\obat;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ObatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(Auth::user()->obats);
        
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validated = $this->validate($request, [
            'nama_obat' => 'required|max:255',
            'stok' => 'required|max:255',
        ]);
        $obat = new obat($validated);
        $obat->user_id = Auth::user()->id;
        $obat->save();
        return response()->json($obat);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\obat  $obat
     * @return \Illuminate\Http\Response
     */
    public function show(obat $obat)
    {
        //
        $obat = obat::where('id', $id)->where('user_id', Auth::user()->id)->first();
        if (empty($obat)){
            abort(404, "Data Tidak Ditemukan");
        }
        return response()->json($obat);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\obat  $obat
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, obat $obat)
    {
        //
        $obat = obat::where('id', $id)->where('user_id', Auth::user()->id)->first();
        if (empty($obat)){
            abort(404, "Data Tidak Ditemukan");
        }
        $validated = $this->validate($request, [
            'title' => 'required|max:255',
            'description' => 'required|max:255',
        ]);
        $obat->title = $validated['title'];
        $obat->description = $validated['description'];
        $obat->save();
        return response()->json($obat);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\obat  $obat
     * @return \Illuminate\Http\Response
     */
    public function destroy(obat $obat)
    {
        //
        $obat = obat::where('id', $id)->where('user_id', Auth::user()->id)->first();
        if (empty($obat)){
            abort(404, "Data Tidak Ditemukan");
        }
        $obat->delete();
        return response()->json(['message' => 'Data Terhapus']);
    }
}