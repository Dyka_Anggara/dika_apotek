<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class obat extends Model
{
    protected $fillable = [
        'nama_obat', 'stok'
    ];
    
    //

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
