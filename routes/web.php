<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->post('register', 'AuthController@register');
$router->post('login', 'AuthController@login');

$router->group(['prefix' => 'obat', 'middleware' => 'auth'], function() use($router){
    $router->get('/', 'ObatController@index');
    $router->post('/', 'ObatController@store');
    $router->get('/{id}', 'ObatController@show');
    $router->put('/{id}', 'ObatController@update');
    $router->delete('/{id}', 'ObatController@destroy');
});